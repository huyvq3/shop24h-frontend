//=================================================admin page login and log out=============================
const token = getCookie("token");

const baseUrlApi = "http://localhost:8088";
const baseUrlXampp = "http://localhost/shop";

$("#showBody").prop("class", "hold-transition sidebar-mini layout-fixed hide");
if(token === "" || token !== "032654987"){
    window.location.href = baseUrlXampp + "/signin.html";
}else {
    $("#showBody").prop("class", "hold-transition sidebar-mini layout-fixed");
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getUserInfo(token) {
    $.ajax({
        url: baseUrlApi + "/user/userbyphonenumber/"+token,
        type: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + token
        },
        success: function (pRes) {
            console.log(pRes);
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    });
}

if (token) {
    $("#logindiv").remove();
    $("#userLogin").append(
        `<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="logout">
            <span class="dropdown-header">Thông tin của tôi</span>
            <div class="dropdown-divider"></div>
            <a class="btn dropdown-item" id="btn-logout">
                <i class="fas fa-file mr-2"></i>Đăng xuất
            </a>
        </div>
          `
    );
    //getUserInfo(token);
}

$("#btn-logout").on("click", function () {
    redirectToLogin();
});

function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie("token", "", 1);
    window.location.href = baseUrlXampp + "/signin.html";
}



//=========================================================================================================
// Summernote
$('#summernote').summernote()
$("#productTab").prop("class", "content-wrapper hide");
$("#customerTab").prop("class", "content-wrapper hide");
$("#orderTab").prop("class", "content-wrapper hide");
$("#customerReportTab").prop("class", "content-wrapper hide");
$("#rateTab").prop("class", "content-wrapper hide");


$("#openRate").on("click", function () {
    
    $("#orderReportTab").prop("class", "content-wrapper hide");
    $("#customerReportTab").prop("class", "content-wrapper hide");
    $("#productTab").prop("class", "content-wrapper hide");
    $("#customerTab").prop("class", "content-wrapper hide");
    $("#orderTab").prop("class", "content-wrapper hide");
    $("#rateTab").prop("class", "content-wrapper");

    $("#openProduct").prop("class", "nav-link");
    $("#openOrder").prop("class", "nav-link");
    $("#openCustomer").prop("class", "nav-link");
    $("#openRate").prop("class", "nav-link btn active");
    loadTableRate();

});

$("#openProduct").on("click", function () {
    
    $("#orderReportTab").prop("class", "content-wrapper hide");
    $("#customerReportTab").prop("class", "content-wrapper hide");
    $("#productTab").prop("class", "content-wrapper");
    $("#customerTab").prop("class", "content-wrapper hide");
    $("#orderTab").prop("class", "content-wrapper hide");
    $("#rateTab").prop("class", "content-wrapper hide");

    $("#openProduct").prop("class", "nav-link btn active");
    $("#openOrder").prop("class", "nav-link");
    $("#openCustomer").prop("class", "nav-link");
    $("#openRate").prop("class", "nav-link");
    loadTableProduct();

});
$("#openCustomer").on("click", function () {
    $("#orderReportTab").prop("class", "content-wrapper hide");
    $("#customerReportTab").prop("class", "content-wrapper hide");
    $("#productTab").prop("class", "content-wrapper hide");
    $("#customerTab").prop("class", "content-wrapper");
    $("#orderTab").prop("class", "content-wrapper hide");
    $("#rateTab").prop("class", "content-wrapper hide");


    $("#openRate").prop("class", "nav-link");
    $("#openProduct").prop("class", "nav-link");
    $("#openOrder").prop("class", "nav-link");
    $("#openCustomer").prop("class", "nav-link btn active");
    loadTableCustomer();
});
$("#openOrder").on("click", function () {
    $("#orderReportTab").prop("class", "content-wrapper hide");
    $("#customerReportTab").prop("class", "content-wrapper hide");
    $("#productTab").prop("class", "content-wrapper hide");
    $("#customerTab").prop("class", "content-wrapper hide");
    $("#orderTab").prop("class", "content-wrapper");
    $("#rateTab").prop("class", "content-wrapper hide");

    $("#openRate").prop("class", "nav-link");
    $("#openProduct").prop("class", "nav-link");
    $("#openCustomer").prop("class", "nav-link");
    $("#openOrder").prop("class", "nav-link btn active");
    loadTableOrder();
    loadTableOrderDetail()
});
$("#openReportOrder").on("click", function () {
    $("#orderReportTab").prop("class", "content-wrapper");
    $("#customerReportTab").prop("class", "content-wrapper hide");
    $("#productTab").prop("class", "content-wrapper hide");
    $("#customerTab").prop("class", "content-wrapper hide");
    $("#orderTab").prop("class", "content-wrapper hide");
    $("#rateTab").prop("class", "content-wrapper hide");

    $("#openProduct").prop("class", "nav-link");
    $("#openOrder").prop("class", "nav-link");
    $("#openCustomer").prop("class", "nav-link");
    $("#openRate").prop("class", "nav-link");
});

$("#openReportCustomer").on("click", function () {
    $("#orderReportTab").prop("class", "content-wrapper hide");
    $("#productTab").prop("class", "content-wrapper hide");
    $("#customerTab").prop("class", "content-wrapper hide");
    $("#orderTab").prop("class", "content-wrapper hide");
    $("#customerReportTab").prop("class", "content-wrapper");
    $("#rateTab").prop("class", "content-wrapper hide");


    $("#openProduct").prop("class", "nav-link");
    $("#openOrder").prop("class", "nav-link");
    $("#openCustomer").prop("class", "nav-link");
    $("#openRate").prop("class", "nav-link");
});

/// ==================================Product Management ===========================================================

var isEditProduct = false;
var indexOfProduct = -1;


var tableProduct = $("#product-table").DataTable({
    // Khai báo các cột của datatable (Chú ý tên cột phải giống thuộc tính của object trong mảng đã khai báo)
    "columns": [
        { "data": "id" },
        { "data": "productCode" },
        { "data": "productName" },
        { "data": "productVendor" },
        { "data": "productL.productLine" },
        { "data": "quantityInStock" },
        { "data": "buyPrice" },
        { "data": "action" }
    ],
    "columnDefs": [
        {
            targets: -1,
            defaultContent: "<i class='edit-product fas fa-edit btn btn-primary btn-sm'></i>"
        },
    ]


});

function loadTableProduct() {
    $.ajax({
        url: baseUrlApi + "/product/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            //Xóa toàn bộ dữ liệu đang có của bảng
            tableProduct.clear();

            //Cập nhật data cho bảng 
            tableProduct.rows.add(responseObject);

            //Cập nhật lại giao diện hiển thị bảng
            tableProduct.draw();
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

// sự kiện click vào nút edit
$("#product-table").on("click", ".edit-product", function () {
    console.log("edit");
    isEditProduct = true;
    editButtonProduct(this);
});

$("#product-table").on("click", ".delete-product", function () {
    console.log("delete");
    //Xác định thẻ tr là cha của nút được chọn
    var rowSelected = $(this).parents('tr');

    //Lấy datatable row
    var datatableRow = tableProduct.row(rowSelected);

    //Lấy data của dòng 
    var dataProduct = datatableRow.data();
    if (confirm("bạn có muốn xóa product có id: " + dataProduct.id)) {
        $.ajax({
            url: baseUrlApi + "/product/delete/" + dataProduct.id,
            type: "DELETE",
            dataType: 'json',
            success: function (responseObject) {
                console.log(responseObject);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    loadTableProduct();

});

// editButton 
function editButtonProduct(button) {
    console.log("lấy thông tin product");
    //Xác định thẻ tr là cha của nút được chọn
    var rowSelected = $(button).parents('tr');

    //Lấy datatable row
    var datatableRow = tableProduct.row(rowSelected);

    //Lấy data của dòng 
    var dataProduct = datatableRow.data();

    //lấy id của voucher

    indexOfProduct = data.id;

    console.log(indexOfProduct);
    console.log(dataProduct);

    $("#inputCode").val(dataProduct.productCode);
    $("#inputName").val(dataProduct.productName);


    $("textarea#summernote").summernote("code", dataProduct.productDescription);

    // lấy value  của textarea
    //var markupStr = $('#summernote').summernote('code');

    $("#inputImage").val(dataProduct.productImage);
    $("#productLine").val(dataProduct.productL.id);
    $("#productVendor").val(dataProduct.productVendor);
    $("#quantityInStock").val(dataProduct.quantityInStock);
    $("#buyPrice").val(dataProduct.buyPrice);

}

$("#saveProduct").on("click", function () {
    console.log(isEditProduct);
    saveDataProduct();
    loadTableProduct();
});

function saveDataProduct() {
    if (isEditProduct == true && validateDataProduct()) {
        updateDataProduct(indexOfProduct);
    }
    else {
        insertDataProduct();
    }
}

function insertDataProduct() {
    console.log("save product");
    var productCode = $("#inputCode").val().trim();
    var productName = $("#inputName").val().trim();


    var productDescription = $('#summernote').summernote('code');
    var productImage = $("#inputImage").val().trim();

    var productLine = $("#productLine").val();
    var productVendor = $("#productVendor").val();
    var quantity = $("#quantityInStock").val().trim();
    var price = $("#buyPrice").val().trim();

    if (validateDataProduct()) {
        var product = {
            "productCode": productCode,
            "productName": productName,
            "productDescription": productDescription,
            "productImage": productImage,
            "productVendor": productVendor,
            "quantityInStock": quantity,
            "buyPrice": price,
        }
        console.log(product);
        $.ajax({
            url: baseUrlApi + "/product/create/" + productLine,
            type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(product),
            dataType: 'json',
            success: function (res) {
                console.log(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }

        });

    }
    loadTableProduct();
}

function updateDataProduct(productId) {

    var productCode = $("#inputCode").val().trim();
    var productName = $("#inputName").val().trim();


    var productDescription = $('#summernote').summernote('code');
    var productImage = $("#inputImage").val().trim();

    var productLine = $("#productLine").val();
    var productVendor = $("#productVendor").val();
    var quantity = $("#quantityInStock").val().trim();
    var price = $("#buyPrice").val().trim();
    console.log(productLine);
    productL = [
        {
            "id": 1,
            "productLine": "Bluetooth",
            "description": "Tai nghe Bluetooth hay tai nghe không dây là thiết bị truyền âm thanh từ nguồn phát (máy nghe nhạc, điện thoại, máy tính bảng) nhờ công nghệ truyền dẫn không dây Bluetooth."
        },
        {
            "id": 2,
            "productLine": "Wire",
            "description": "Loại tai nghe có thiết kế phần củ loa (driver) với ống dẫn âm nhỏ gọn, thuôn dài, dễ dàng tiến sâu vào trong tai để truyền âm nhanh và cách âm tốt hơn."
        }
    ]

    var product = {
        "productCode": productCode,
        "productName": productName,
        "productDescription": productDescription,
        "productImage": productImage,
        "productVendor": productVendor,
        "quantityInStock": quantity,
        "buyPrice": price,
        "productL": productL
    }
    if (productLine == 1) {
        console.log(productLine);
        product.productL = productL[0];
        $.ajax({
            url: baseUrlApi + "/product/update/" + productId,
            type: 'PUT',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(product),
            dataType: 'json',
            success: function (res) {
                console.log(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }

        });

    }
    else if (productLine == 2) {
        product.productL = productL[1]
        $.ajax({
            url: baseUrlApi + "/product/update/" + productId,
            type: 'PUT',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(product),
            dataType: 'json',
            success: function (res) {
                console.log(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }

        });
    }
    indexOfProduct = -1;
    isEditProduct = false;
    loadTableCustomer();
}

function validateDataProduct() {
    var productCode = $("#inputCode").val().trim();
    var productName = $("#inputName").val().trim();


    var markupStr = $('#summernote').summernote('code');
    var productImage = $("#inputImage").val().trim();

    var productLine = $("#productLine").val().trim();
    var productVendor = $("#productVendor").val().trim();
    var quantity = $("#quantityInStock").val().trim();
    var price = $("#buyPrice").val().trim();

    var check = false;
    if (productCode === "" && productName === "") {
        alert("bạn đang để trống mã product hoặc tên");
        return check;
    }
    if (markupStr == "" && productImage == "") {
        alert("bạn đang để trống description hoặn link hình ảnh");
        return check;
    }
    if (productLine == "0" && productVendor == "0") {
        alert("bạn chưa chọn loại sản phẩm hoặc thương hiệu cho sản phẩm");
        return check;
    }
    if (quantity == "" && price == "") {
        alert("bạn chưa nhập số lượng hoặc giá");
        return check;
    }

    check = true;
    return check;

}

function reloadFormProduct() {
    $("#inputCode").val("");
    $("#inputName").val("");


    $("textarea#summernote").summernote("code", "");
    $("#inputImage").val("");

    $("#productLine").val("0");
    $("#productVendor").val("0");
    $("#quantityInStock").val("");
    $("#buyPrice").val("");
}

$("#cancle").on("click", function () {
    reloadFormProduct();
});

//=============================================================================================================


/// ======================================Customer Management ===========================================================
var isEditCustomer = false;
var indexOfCustomer = -1;


var tableCustomer = $("#customer-table").DataTable({
    // Khai báo các cột của datatable (Chú ý tên cột phải giống thuộc tính của object trong mảng đã khai báo)
    "columns": [
        { "data": "id" },
        { "data": "fullName" },
        { "data": "birthday" },
        { "data": "phoneNumber" },
        { "data": "email" },
        { "data": "city" },
        { "data": "state" },
        { "data": "address" },
        { "data": "action" }
    ],
    "columnDefs": [
        {
            targets: -1,
            defaultContent: "<i class='edit-customer fas fa-edit btn btn-primary btn-sm'></i>"
        },
    ]


});

function loadTableCustomer() {
    $.ajax({
        url: baseUrlApi + "/customer/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            //Xóa toàn bộ dữ liệu đang có của bảng
            tableCustomer.clear();

            //Cập nhật data cho bảng 
            tableCustomer.rows.add(responseObject);

            //Cập nhật lại giao diện hiển thị bảng
            tableCustomer.draw();
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}


// sự kiện click vào nút edit
$("#customer-table").on("click", ".edit-customer", function () {
    console.log("edit");
    isEditCustomer = true;
    editButtonCustomer(this);
});

// editButton 
function editButtonCustomer(button) {
    console.log("lấy thông tin customer");
    //Xác định thẻ tr là cha của nút được chọn
    var rowSelected = $(button).parents('tr');

    //Lấy datatable row
    var datatableRow = tableCustomer.row(rowSelected);

    //Lấy data của dòng 
    var dataCustomer = datatableRow.data();

    //lấy id của voucher

    indexOfCustomer = dataCustomer.id;

    console.log(indexOfCustomer);
    console.log(dataCustomer);

    $("#inputLastName").val(dataCustomer.lastName);
    $("#inputFirstName").val(dataCustomer.firstName);
    $("#inputFullName").val(dataCustomer.fullName);
    $("#inputBirthDay").val(dataCustomer.birthday.split("-").reverse().join("-"));
    $("#citySelect option:selected").text(dataCustomer.city);
    $("#stateSelect option:selected").text(dataCustomer.state);
    $("#inputAdress").val(dataCustomer.address);
    $("#inputEMail").val(dataCustomer.email)
    $("#inputPhone").val(dataCustomer.phoneNumber);
}

$("#saveCustomer").on("click", function () {
    console.log(isEditCustomer);
    saveDataCustomer();
    loadTableCustomer();
});

function saveDataCustomer() {
    if (isEditCustomer == true && validateDataCustomer()) {
        updateDataCustomer(indexOfCustomer);
    }
    else {
        insertDataCustomer();
    }
}

function insertDataCustomer() {
    console.log("save customer");
    var lastName = $("#inputLastName").val().trim();
    var firstName = $("#inputFirstName").val().trim();
    var fullName = $("#inputFullName").val().trim();
    var birthday = $("#inputBirthDay").val().split("-").reverse().join("-");
    var citySelect = $("#citySelect option:selected").text();
    var stateSelect = $("#stateSelect option:selected").text();
    var address = $("#inputAdress").val().trim();
    var email = $("#inputEMail").val().trim();
    var phoneNumber = $("#inputPhone").val().trim();


    if (validateDataCustomer()) {

        var customer = {
            "lastName": lastName,
            "firstName": firstName,
            "fullName": fullName,
            "birthday": birthday,
            "city": citySelect,
            "state": stateSelect,
            "address": address,
            "email" : email,
            "phoneNumber" : phoneNumber
        }

        $.ajax({
            url: baseUrlApi + "/customer/create",
            type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(customer),
            dataType: 'json',
            success: function (res) {
                console.log(res);
                loadTableCustomer();
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }

        });

    }
    
    reloadFormCustomer()
}

function updateDataCustomer(customerId) {

    var lastName = $("#inputLastName").val().trim();
    var firstName = $("#inputFirstName").val().trim();
    var fullName = $("#inputFullName").val().trim();
    var birthday = $("#inputBirthDay").val().split("-").reverse().join("-");
    var citySelect = $("#citySelect option:selected").text();
    var stateSelect = $("#stateSelect option:selected").text();
    var address = $("#inputAdress").val().trim();
    var email = $("#inputEMail").val().trim();
    var phoneNumber = $("#inputPhone").val().trim();

    console.log(birthday);

    var customer = {
        "lastName": lastName,
        "firstName": firstName,
        "fullName": fullName,
        "birthday": birthday,
        "city": citySelect,
        "state": stateSelect,
        "address": address,
        "email" : email,
        "phoneNumber" : phoneNumber
    }
    console.log(customer);
    $.ajax({
        url: baseUrlApi + "/customer/update/" + customerId,
        type: 'PUT',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(customer),
        dataType: 'json',
        success: function (res) {
            console.log(res);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText);
        }

    });
    loadTableCustomer();
    reloadFormCustomer()
    indexOfCustomer = -1;
    isEditCustomer = false;
    
}

function validateDataCustomer() {
    var lastName = $("#inputLastName").val().trim();
    var firstName = $("#inputFirstName").val().trim();
    var fullName = $("#inputFullName").val().trim();
    var birthday = $("#inputBirthDay").val();
    var citySelect = $("#citySelect option:selected").text();
    var stateSelect = $("#stateSelect option:selected").text();
    var address = $("#inputAdress").val().trim();
    var email = $("#inputEMail").val().trim();
    var phoneNumber = $("#inputPhone").val().trim();


    var check = false;
    if (lastName === "" && firstName === "") {
        alert("bạn đang chưa nhập họ hoặc tên");
        return check;
    }
    if (fullName == "" && birthday == "") {
        alert("bạn chưa nhập họ và tên hoặc ngày sinh");
        return check;
    }
    if (citySelect == "0" && stateSelect == "0") {
        alert("bạn chưa chọn thành phố hoặc quận huyện");
        return check;
    }
    if (address == "" && email == "") {
        alert("bạn chưa nhập địa chỉ hoặc email");
        return check;
    }
    if (phoneNumber == "") {
        alert("bạn chưa nhập số điện thoại");
        return check;
    }

    check = true;
    return check;

}

function reloadFormCustomer() {
    console.log($("#citySelect option:selected").text());
    $("#inputLastName").val("");
    $("#inputFirstName").val("");
    $("#inputFullName").val("");
    $("#inputBirthDay").val("");
    $("#citySelect").val("");
    $("#stateSelect").val("");
    $("#inputAdress").val("");
    $("#inputEMail").val("")
    $("#inputPhone").val("")

}

$("#cancleCustomer").on("click", function () {
    reloadFormCustomer();
});
//=============================================================================================================


/// ======================================Order Management ===========================================================
var isEditOrder = false;
var indexOfOrder = -1;


var tableOrder = $("#order-table").DataTable({
    // Khai báo các cột của datatable (Chú ý tên cột phải giống thuộc tính của object trong mảng đã khai báo)
    "columns": [
        { "data": "id" },
        { "data": "orderDate" },
        { "data": "requiredDate" },
        { "data": "shippedDate" },
        { "data": "status" },
        { "data": "comments" },
        { "data": "customer.fullName"},
        { "data": "customer.id"},
        { "data": "action" },
    ],
    "columnDefs": [
        {
            targets: -1,
            defaultContent: "<i class='edit-order fas fa-edit btn btn-primary btn-sm'></i>"
        },
    ]


});

function loadTableOrder() {
    $.ajax({
        url: baseUrlApi + "/order/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            //Xóa toàn bộ dữ liệu đang có của bảng
            tableOrder.clear();

            //Cập nhật data cho bảng 
            tableOrder.rows.add(responseObject);

            //Cập nhật lại giao diện hiển thị bảng
            tableOrder.draw();
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}


// sự kiện click vào nút edit
$("#order-table").on("click", ".edit-order", function () {
    console.log("edit");
    isEditOrder = true;
    editButtonOrder(this);
});

// editButton 
function editButtonOrder(button) {
    console.log("lấy thông tin order");
    //Xác định thẻ tr là cha của nút được chọn
    var rowSelected = $(button).parents('tr');

    //Lấy datatable row
    var datatableRow = tableOrder.row(rowSelected);

    //Lấy data của dòng 
    var dataOrder = datatableRow.data();

    //lấy id của voucher

    indexOfOrder = dataOrder.id;

    console.log(indexOfOrder);
    console.log(tableOrder);

    $("#inputOrderDate").val(dataOrder.orderDate.split("-").reverse().join("-"));
    $("#inputRequiredDate").val(dataOrder.requiredDate.split("-").reverse().join("-"));
    $("#inputShippedDate").val(dataOrder.shippedDate.split("-").reverse().join("-"));
    $("#inputStatus").val(dataOrder.status);
    $("#inputComments").val(dataOrder.comments)
    $("#inputCustomerId").val(dataOrder.customer.id);
}

$("#saveOrder").on("click", function () {
    console.log(isEditOrder);
    saveDataOrder();
    loadTableOrder();
});

function saveDataOrder() {
    if (isEditOrder == true && validateDataOrder()) {
        updateDataOrder(indexOfOrder);
    }
    else {
        insertDataOrder();
    }
}

function insertDataOrder() {
    console.log("save order");
    var orderDate = $("#inputOrderDate").val().split("-").reverse().join("-");
    var requiredDate = $("#inputRequiredDate").val().split("-").reverse().join("-");
    var shippedDate = $("#inputShippedDate").val().split("-").reverse().join("-");
    var status = $("#inputStatus").val();
    var comments = $("#inputComments").val()
    var customerId = $("#inputCustomerId").val();


    if (validateDataOrder()) {

        var order = {
            "orderDate": orderDate,
            "requiredDate": requiredDate,
            "shippedDate": shippedDate,
            "status": status,
            "comments": comments,
        }

        $.ajax({
            url: baseUrlApi + "/order/create/"+customerId,
            type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(order),
            dataType: 'json',
            success: function (res) {
                console.log(res);
                loadTableOrder();
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }

        });

    }
    
    reloadFormOrder()
}

function updateDataOrder(orderId) {

    var orderDate = $("#inputOrderDate").val().split("-").reverse().join("-");
    var requiredDate = $("#inputRequiredDate").val().split("-").reverse().join("-");
    var shippedDate = $("#inputShippedDate").val().split("-").reverse().join("-");
    var status = $("#inputStatus").val();
    var comments = $("#inputComments").val()
    var customerId = $("#inputCustomerId").val();


    var order = {
        "orderDate": orderDate,
        "requiredDate": requiredDate,
        "shippedDate": shippedDate,
        "status": status,
        "comments": comments,
    }

    console.log(order);
    $.ajax({
        url: baseUrlApi + "/order/update/" + orderId +"/"+customerId ,
        type: 'PUT',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(order),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            loadTableOrder();
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText);
        }

    });
    loadTableOrder();
    reloadFormOrder()
    indexOfOrder = -1;
    isEditOrder = false;
    
}

function validateDataOrder() {
    var orderDate = $("#inputOrderDate").val().split("-").reverse().join("-");
    var requiredDate = $("#inputRequiredDate").val().split("-").reverse().join("-");
    var shippedDate = $("#inputShippedDate").val().split("-").reverse().join("-");
    var status = $("#inputStatus").val();
    var comments = $("#inputComments").val()
    var customerId = $("#inputCustomerId").val();


    var check = false;
    if (orderDate === "" && requiredDate === "") {
        alert("bạn đang chưa nhập họ hoặc tên");
        return check;
    }
    if (shippedDate == "" && status == "") {
        alert("bạn chưa nhập họ và tên hoặc ngày sinh");
        return check;
    }
    if (comments == "0" && customerId == "0") {
        alert("bạn chưa chọn thành phố hoặc quận huyện");
        return check;
    }

    check = true;
    return check;

}

function reloadFormOrder() {
    $("#inputOrderDate").val("");
    $("#inputRequiredDate").val("");
    $("#inputShippedDate").val("");
    $("#inputStatus").val("");
    $("#inputComments").val("")
    $("#inputCustomerId").val("");
}

$("#cancleOrder").on("click", function () {
    reloadFormOrder();
});
//=============================================================================================================

/// ======================================OrderDetail Management ===========================================================
var isEditOrderDetail = false;
var indexOfOrderDetail = -1;


var tableOrderDetail = $("#orderdetail-table").DataTable({
    // Khai báo các cột của datatable (Chú ý tên cột phải giống thuộc tính của object trong mảng đã khai báo)
    "columns": [
        { "data": "id" },
        { "data": "order.id" },
        { "data": "product.productName" },
        { "data": "quantityOrder" },
        { "data": "product.buyPrice" },
        { "data": "action" },
    ],
    "columnDefs": [
        {
            targets: -1,
            defaultContent: "<i class='edit-orderdetail fas fa-edit btn btn-primary btn-sm'></i>"
        },
    ]


});
// inputOrderId
// inputProductId
// inputQuantityOrder
// inputPrice
// cancleOrderDetail
// saveOrderDetail
function loadTableOrderDetail() {
    $.ajax({
        url: baseUrlApi + "/orderdetail/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            //Xóa toàn bộ dữ liệu đang có của bảng
            tableOrderDetail.clear();

            //Cập nhật data cho bảng 
            tableOrderDetail.rows.add(responseObject);

            //Cập nhật lại giao diện hiển thị bảng
            tableOrderDetail.draw();
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}


// sự kiện click vào nút edit
$("#orderdetail-table").on("click", ".edit-orderdetail", function () {
    console.log("edit");
    isEditOrderDetail = true;
    editButtonOrderDetail(this);
});

// editButton 
function editButtonOrderDetail(button) {
    console.log("lấy thông tin order");
    //Xác định thẻ tr là cha của nút được chọn
    var rowSelected = $(button).parents('tr');

    //Lấy datatable row
    var datatableRow = tableOrderDetail.row(rowSelected);

    //Lấy data của dòng 
    var dataOrderDetail = datatableRow.data();

    //lấy id của voucher

    indexOfOrderDetail = dataOrderDetail.id;

    console.log(indexOfOrderDetail);
    console.log(tableOrderDetail);

    $("#inputOrderId").val(dataOrderDetail.order.id);
    $("#inputProductId").val(dataOrderDetail.product.id);
    $("#inputQuantityOrder").val(dataOrderDetail.quantityOrder);
    $("#inputPrice").val(dataOrderDetail.priceEach);

}

$("#saveOrderDetail").on("click", function () {
    console.log(isEditOrderDetail);
    saveDataOrderDetail();
    loadTableOrderDetail();
});

function saveDataOrderDetail() {
    if (isEditOrderDetail == true && validateDataOrderDetail()) {
        updateDataOrderDetail(indexOfOrderDetail);
    }
    else {
        insertDataOrderDetail();
    }
}

function insertDataOrderDetail() {
    console.log("save order");
    var orderId = $("#inputOrderId").val();
    var productId = $("#inputProductId").val();
    var quantity = $("#inputQuantityOrder").val();


    if (validateDataOrderDetail()) {

        var orderDetail = {
            "quantityOrder": quantity,
        }

        $.ajax({
            url: baseUrlApi + "/orderdetail/create/" + orderId + "/" + productId,
            type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(orderDetail),
            dataType: 'json',
            success: function (res) {
                console.log(res);
                loadTableOrderDetail();
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText);
            }

        });

    }
    
    reloadFormOrderDetail()
}

function updateDataOrderDetail(orderDetailId) {

    var orderId = $("#inputOrderId").val().trim();
    var productId = $("#inputProductId").val().trim();
    var quantity = $("#inputQuantityOrder").val().trim();

    var orderDetail = {
        "quantityOrder" : quantity
    }

    console.log(orderDetail);
    $.ajax({
        url: baseUrlApi + "/orderdetail/update/"+ orderDetailId+ "/"+ orderId+ "/"+ productId,
        type: 'PUT',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(orderDetail),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            loadTableOrderDetail();
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText);
        }

    });
    loadTableOrderDetail();
    reloadFormOrderDetail()
    indexOfOrderDetail = -1;
    isEditOrderDetail = false;
    
}

function validateDataOrderDetail() {
    var orderId = $("#inputOrderId").val().trim();
    var productId = $("#inputProductId").val().trim();
    var quantity = $("#inputQuantityOrder").val().trim();

    var check = false;
    if (orderId === "" && productId === "") {
        alert("bạn chưa nhập id hóa đơn hoặc id sản phẩm");
        return check;
    }
    if (quantity == "") {
        alert("bạn chưa nhập số lượng");
        return check;
    }

    check = true;
    return check;

}

function reloadFormOrderDetail() {
    $("#inputOrderId").val("");
    $("#inputProductId").val("");
    $("#inputQuantityOrder").val("");
}

$("#cancleOrderDetail").on("click", function () {
    reloadFormOrderDetail();
});
//=============================================================================================================

//============================================= Rate tab ====================================================

const tableRate = $("#rate-table").DataTable({
    // Khai báo các cột của datatable (Chú ý tên cột phải giống thuộc tính của object trong mảng đã khai báo)
    "columns": [
        { "data": "id" },
        { "data": "rateScore" },
        { "data": "comments" },
        { "data": "orderdetails.product.productName" },
        { "data": "orderdetails.product.productCode" },
        { "data": "orderdetails.order.customer.fullName" },
        { "data": "orderdetails.order.customer.phoneNumber" },
        { "data": "updateDate" },
        { "data": "action" },
    ],
    "columnDefs": [
        {
            targets: -1,
            defaultContent: "<i class='delete-rate fas fa-trash-alt btn btn-danger btn-sm'></i>"
        },
    ]


});

function loadTableRate() {
    $.ajax({
        url: baseUrlApi + "/rate/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            //Xóa toàn bộ dữ liệu đang có của bảng
            tableRate.clear();

            //Cập nhật data cho bảng 
            tableRate.rows.add(responseObject);

            //Cập nhật lại giao diện hiển thị bảng
            tableRate.draw();
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}


// sự kiện click vào nút edit
$("#rate-table").on("click", ".delete-rate", function () {
    console.log("delete");
    //Xác định thẻ tr là cha của nút được chọn
    var rowSelected = $(this).parents('tr');

    //Lấy datatable row
    var datatableRow = tableRate.row(rowSelected);

    //Lấy data của dòng 
    var dataRate = datatableRow.data();
    var rateId = dataRate.id;
    if (confirm("bạn có muốn xóa phần đánh giá có id: " + rateId)) {
        $.ajax({
            url: baseUrlApi + "/rate/delete/" + rateId,
            type: "DELETE",
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                console.log(responseObject);
                loadTableRate();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }


});
//=============================================================================================================