//==================== report every day===========================
var day = [];
var sumMoney = [];

var date = new Date();
var today = date.getDate();
var dayLoadFirst = [];
var sumMoneyLoadFirst = [];



// hiện sẵn báo cáo tổng tiền của 7 ngày trước 
$.ajax({
  url: "http://localhost:8088/order/datereport/" + (today-7) + "/" + (today-1),
  type: "GET",
  dataType: 'json',
  async: false,
  success: function (res) {
    console.log(res);
    for (let index = 0; index < res.length; index++) {
      const element = res[index];
      dayLoadFirst.push(element[0]);
      sumMoneyLoadFirst.push(element[3]);
    }
    console.log(sumMoneyLoadFirst);
    var areaChartDataOrder = {
      labels: dayLoadFirst,
      datasets: [
        {
          label: 'Tổng tiền theo từng ngày',
          backgroundColor: 'rgba(60,141,188,0.9)',
          borderColor: 'rgba(60,141,188,0.8)',
          pointRadius: false,
          pointColor: '#3b8bba',
          pointStrokeColor: 'rgba(60,141,188,1)',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data: sumMoneyLoadFirst
        }
      ]
    }
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barDateSum').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartDataOrder)
    var temp0 = areaChartDataOrder.datasets[0]
    // var temp1 = areaChartDataOrder.datasets[1]
    barChartData.datasets[0] = temp0

    var barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })


    var donutData = {
      labels: [
        'Chrome',
        'IE',
        'FireFox',
        'Safari',
        'Opera',
        'Navigator',
      ],
      datasets: [
        {
          data: [700, 500, 400, 600, 300, 100],
          backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }

   
  },
  error: function (error) {
    console.log(error);
  }
});

// khi người dùng chọn lọc ngày thì sẽ hiện theo những ngày mà người dùng chọn
$('#reservation').on('apply.daterangepicker', function (ev, picker) {
  $("#barDateSum").html("");

  $("#exceltotalmoneyeday").on("click", function(){
    var dateBegin = picker.startDate.format('DD')
    var dateEnd = picker.endDate.format('DD');
    console.log(dateBegin, dateEnd);
    window.location.href = "http://localhost:8088/order/datereportexcell/"+dateBegin+"/"+dateEnd;
  });

  day = [];
  sumMoney = [];
  var dateBegin = picker.startDate.format('DD')
  var dateEnd = picker.endDate.format('DD');

  console.log(picker.startDate);
  console.log(picker.startDate.format('YYYY-MM-DD'));
  console.log(picker.endDate.format('YYYY-MM-DD'));

  $.ajax({
    url: "http://localhost:8088/order/datereport/" + dateBegin + "/" + dateEnd,
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (res) {
      console.log(res);
      for (let index = 0; index < res.length; index++) {
        const element = res[index];
        day.push(element[0]);
        sumMoney.push(element[3]);
        // areaChartDataOrder.labels.push(element[0]);
        // areaChartDataOrder.datasets[0].data.push(element[2]);
      }
      console.log(day, sumMoney);
      var areaChartDataOrder = {
        labels: day,
        datasets: [
          {
            label: 'Tổng tiền theo từng ngày',
            backgroundColor: 'rgba(60,141,188,0.9)',
            borderColor: 'rgba(60,141,188,0.8)',
            pointRadius: false,
            pointColor: '#3b8bba',
            pointStrokeColor: 'rgba(60,141,188,1)',
            pointHighlightFill: '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data: sumMoney
          }
        ]
      }
      //-------------
      //- BAR CHART -
      //-------------
      var barChartCanvas = $('#barDateSum').get(0).getContext('2d')
      var barChartData = $.extend(true, {}, areaChartDataOrder)
      var temp0 = areaChartDataOrder.datasets[0]
      // var temp1 = areaChartDataOrder.datasets[1]
      barChartData.datasets[0] = temp0

      var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
      }

      new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
      })


      var donutData = {
        labels: [
          'Chrome',
          'IE',
          'FireFox',
          'Safari',
          'Opera',
          'Navigator',
        ],
        datasets: [
          {
            data: [700, 500, 400, 600, 300, 100],
            backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
          }
        ]
      }
    },
    error: function (error) {
      console.log(error);
    }
  });

});

//=================================================================

//========================= load every week =========================
var week = [];
var sumMoneyWeek = [];

var d = new Date();
var thisYear = d.getFullYear();

var weekLoadFirst = [];
var sumMoneyWeekLoadFirst = [];

// load ra dữ liệu ra front end khi người dùng chưa lọc
$.ajax({
  url: "http://localhost:8088/order/weekreport/" + thisYear,
  type: "GET",
  dataType: 'json',
  async: false,
  success: function (res) {
    console.log(res);
    for (let index = 0; index < res.length; index++) {
      const element = res[index];
      weekLoadFirst.push("Tuần "+element[0]);
      sumMoneyWeekLoadFirst.push(element[2]);
      // areaChartDataOrder.labels.push(element[0]);
      // areaChartDataOrder.datasets[0].data.push(element[2]);
    }
    console.log(day, sumMoney);
    var areaChartDataOrderWeek = {
      labels: weekLoadFirst,
      datasets: [
        {
          label: 'Tổng tiền theo từng tuần',
          backgroundColor: 'rgba(60,141,188,0.9)',
          borderColor: 'rgba(60,141,188,0.8)',
          pointRadius: false,
          pointColor: '#3b8bba',
          pointStrokeColor: 'rgba(60,141,188,1)',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data: sumMoneyWeekLoadFirst
        }
      ]
    }
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barDateSumWeek').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartDataOrderWeek)
    var temp0 = areaChartDataOrderWeek.datasets[0]
    // var temp1 = areaChartDataOrder.datasets[1]
    barChartData.datasets[0] = temp0

    var barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })


    var donutData = {
      labels: [
        'Chrome',
        'IE',
        'FireFox',
        'Safari',
        'Opera',
        'Navigator',
      ],
      datasets: [
        {
          data: [700, 500, 400, 600, 300, 100],
          backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
  },
  error: function (error) {
    console.log(error);
  }
});

$("#daterangeforweek").on("change",function(){
  $("#barDateSumWeek").html("");
  week = [];
  sumMoneyWeek = [];

  var yearValue = parseInt($("#daterangeforweek").val().trim());
  $("#exceltotalmoneyeweek").on("click", function(){
    window.location.href = "http://localhost:8088/order/yearreportexcell/"+yearValue;
  });

  $.ajax({
    url: "http://localhost:8088/order/weekreport/" + yearValue,
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (res) {
      console.log(res);
      for (let index = 0; index < res.length; index++) {
        const element = res[index];
        week.push("Tuần "+element[0]);
        sumMoneyWeek.push(element[2]);
        // areaChartDataOrder.labels.push(element[0]);
        // areaChartDataOrder.datasets[0].data.push(element[2]);
      }
      console.log(day, sumMoney);
      var areaChartDataOrderWeek = {
        labels: week,
        datasets: [
          {
            label: 'Tổng tiền theo từng tuần',
            backgroundColor: 'rgba(60,141,188,0.9)',
            borderColor: 'rgba(60,141,188,0.8)',
            pointRadius: false,
            pointColor: '#3b8bba',
            pointStrokeColor: 'rgba(60,141,188,1)',
            pointHighlightFill: '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data: sumMoneyWeek
          }
        ]
      }
      //-------------
      //- BAR CHART -
      //-------------
      var barChartCanvas = $('#barDateSumWeek').get(0).getContext('2d')
      var barChartData = $.extend(true, {}, areaChartDataOrderWeek)
      var temp0 = areaChartDataOrderWeek.datasets[0]
      // var temp1 = areaChartDataOrder.datasets[1]
      barChartData.datasets[0] = temp0

      var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
      }

      new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
      })


      var donutData = {
        labels: [
          'Chrome',
          'IE',
          'FireFox',
          'Safari',
          'Opera',
          'Navigator',
        ],
        datasets: [
          {
            data: [700, 500, 400, 600, 300, 100],
            backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
          }
        ]
      }
    },
    error: function (error) {
      console.log(error);
    }
  });
});

//====================================================================

$("#exceltotalmoneyeday").on("click", function(){
  
});


