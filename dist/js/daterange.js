//Date range picker
$('#reservation').daterangepicker()

//  //Date range picker with time picker
//  $('#reservationtime').daterangepicker({
//     timePicker: true,
//     timePickerIncrement: 30,
//     locale: {
//       format: 'MM/DD/YYYY hh:mm A'
//     }
// })

$('#daterangeforweek').each(function () {
  $(this).datepicker({
      autoclose: true,
      format: " yyyy",
      viewMode: "years",
      minViewMode: "years"
  });
  $(this).datepicker('clearDates');
});