//===========================================login and logout==================================================
const token = getCookie("token");
const baseUrlApi = "http://localhost:8088";
const baseUrlXampp = "http://localhost/shop";

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

var getUser = [];
function getUserInfo(token) {
    $.ajax({
        url: baseUrlApi +"/user/userbyphonenumber/" + token,
        type: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        headers: {
            "Authorization": "Bearer " + token
        },
        success: function (pRes) {
            console.log(pRes);
            getUser.push(pRes);
            $("#hoten").val(pRes.customer.fullName);
            $("#sdt").val(pRes.customer.phoneNumber);
            $("#diachi").val(pRes.customer.address);
            $("#email").val(pRes.customer.email);
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    });
}
{/* <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="logout">
    <span class="dropdown-header">Thông tin của tôi</span>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item">
        <i class="fas fa-envelope mr-2"></i>Chi tiết
    </a>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item">
        <i class="fas fa-users mr-2"></i>Đơn hàng của tôi
    </a>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item">
        <i class="fas fa-file mr-2"></i>Đăng xuất
    </a>
</div> */}
if (token) {
    $("#logindiv").remove();
    $("#userLogin").append(
        `<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="logout">
            <span class="dropdown-header">Thông tin của tôi</span>
            <div class="dropdown-divider"></div>
            <a class="btn dropdown-item" id="detailcustomer" data="1">
                <i class="fas fa-envelope mr-2"></i>Tài khoản của tôi
            </a>
            <div class="dropdown-divider"></div>
            <a  class="btn dropdown-item" id="detailorderme" data="2">
                <i class="fas fa-users mr-2"></i>Đơn hàng của tôi
            </a>
            <div class="dropdown-divider"></div>
            <a class="btn dropdown-item" id="btn-logout">
                <i class="fas fa-file mr-2"></i>Đăng xuất
            </a>
        </div>
          `
    );
    getUserInfo(token);
    $("#username").append(getUser[0].username);
}

$("#btn-logout").on("click", function () {
    redirectToLogin();
});

function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie("token", "", 1);
    window.location.href = baseUrlXampp + "/myorder.html";
}

$("#detailcustomer").on("click", function(){
    var data = this.getAttribute("data");
    const detailcustomer = "orderme.html";
    urlSiteToOpen = detailcustomer + "?" +
        "id=" + data;
    window.location.href = urlSiteToOpen;
});

$("#detailorderme").on("click", function(){
    var data = this.getAttribute("data");
    const detailorderme = "orderme.html";
    urlSiteToOpen = detailorderme + "?" +
        "id=" + data;
    window.location.href = urlSiteToOpen;
});
//==============================================================================================================


var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'VND'
});

var totalPay = {
    sum: 0
};

var customer = {
    fullName: "a",
    phoneNumber: 0,
    address: "a"
};

var order = {
    status: "Open",
    comments: ""
}

var orderdetail = {
    orderId: 0,
    productId: 0,
    quantityOrder: 1,
    priceEach: 0
};



listOrderDetail = [];


var getCookies = function () {
    var pairs = document.cookie.split(";");
    var cookies = [];
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split("=");
        cookies[(pair[0] + '').trim()] = unescape(pair.slice(1).join('='));
    }
    return cookies;
}

function deleteCookie(name) { setCookie(name, '', -1); }


var myorder = getCookies();

console.log(myorder);

loadProductSelected(myorder);


function loadProductSelected(myorder) {
    for (let index = 0; index < myorder.length; index++) {
        const element = myorder[index];
        if (typeof element === 'string' || element instanceof String) {
            console.log(element);

            $.ajax({
                url: baseUrlApi + "/product/details/" + element,
                type: "GET",
                dataType: 'json',
                async: false,
                success: function (res) {
                    var formatter = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'VND'
                    });
                    console.log(res);
                    var orderdetailTemp = {
                        orderId: 0,
                        productId: res.id,
                        quantityOrder: 1,
                        priceEach: res.buyPrice
                    };

                    var tr = $("<tr/>");
                    var tdImg = $("<td/>");

                    var img = $("<img/>", { "class": "image" });
                    tdImg.append(img);
                    img.prop("src", baseUrlXampp + res.productImage);

                    var h3Name = $("<h3/>");
                    var tdProductName = $("<td/>");
                    h3Name.html(res.productName);
                    tdProductName.append(h3Name);

                    var h3Price = $("<h3/>");
                    var tdPrice = $("<td/>");
                    h3Price.html(formatter.format(res.buyPrice));

                    totalPay.sum += parseInt(res.buyPrice);
                    tdPrice.append(h3Price);


                    var tdAdd = $("<td/>");
                    var divBtnAdd = $("<div/>", { "class": "buttons_added" });
                    var inputMinus = $("<input/>", { "class": "minus is-form pd", "type": "button", "value": "-" });
                    var inputArea = $("<input/>", {
                        "class": "input-qty", "aria-label": "quantity", "max": "10",
                        "min": "1", "name": "", "type": "number", "value": "1"
                    });
                    var inputPlus = $("<input/>", { "class": "plus is-form pd", "type": "button", "value": "+" });
                    inputMinus.attr("data", res.id);
                    inputArea.attr("id", res.id);
                    inputPlus.attr("data", res.id);
                    divBtnAdd.append(inputMinus, inputArea, inputPlus);
                    tdAdd.append(divBtnAdd);

                    var btn = $("<i/>", { "class": "btn fas fa-trash-alt" });
                    var tdDelete = $("<td/>");
                    var h3Delete = $("<h3/>", { "class": "bamvao" });
                    h3Delete.attr("data", res.id);
                    h3Delete.append(btn);
                    tdDelete.append(h3Delete);

                    tr.append(tdImg, tdProductName, tdPrice, tdAdd, tdDelete);

                    $("#loadProduct").append(tr);
                    orderdetail = orderdetailTemp;
                    listOrderDetail.push(orderdetail);
                },
                error: function (error) {
                    console.log(error);

                }
            });
        }
    }

    var trSum = $("<tr/>");

    var tdEmpty = $("<td/>");

    var tdSumText = $("<td/>");
    h3SumText = $("<h3/>");
    h3SumText.html("Tổng Tiền");
    tdSumText.append(h3SumText);

    tdSumPrice = $("<td/>");
    h3SumPrice = $("<h3/>", { "id": "priceSum" });
    h3SumPrice.attr("data", totalPay.sum)
    h3SumPrice.html(formatter.format(totalPay.sum));
    tdSumPrice.append(h3SumPrice);


    trSum.append(tdEmpty, tdSumText, tdSumPrice);

    $("#loadProduct").append(trSum);
}

console.log(listOrderDetail);


//hàm click vào sản phẩm nào sẽ xóa sản phẩm đó
$('#loadProduct').on('click', '.bamvao', function () {
    var data = this.getAttribute("data");
    console.log(data);
    deleteCookie(data);
    window.location.href = "myorder.html";
});

// các sự kiện tắt modal khi sau khi ấn nút đặt hàng
$("#tat").on("click", function () {
    $('#ModalCenter').modal('hide');
});

$("#xBtn").on("click", function () {
    $('#ModalCenter').modal('hide');
});

$("#closeMsgSuccess").on("click", function () {
    $('#msgSuccess').modal('hide');
});
$("#xCloseMsgSuccess").on("click", function () {
    $('#msgSuccess').modal('hide');
});
//================================================

$("#sendOrder").on("click", function () {
    if(listOrderDetail.length == 0){
        alert("Bạn chưa thêm sản phẩm nào vào giỏ hàng");
    } else {
        $('#ModalCenter').modal('show');
        var vHoTen = $("#hoten").val().trim();
        var vSdt = $("#sdt").val().trim();
        var vDiaChi = $("#diachi").val().trim();
        var vLoiNhan = $("#loinhan").val().trim();
    
        $("#name").html("Họ Tên: " + vHoTen);
        $("#phone").html("Số điện thoại: " + vSdt);
        $("#address").html("Địa chỉ: " + vDiaChi);
    
        $("#toPay").html("Tổng tiền: " + formatter.format(totalPay.sum));
        customer.fullName = vHoTen;
        customer.phoneNumber = vSdt;
        customer.address = vDiaChi;
        order.comments = vLoiNhan;
    }
    
});


$("#confirmOrder").on("click", function () {
    $.ajax({
        url: baseUrlApi +"/customer/customerbyphone/" + customer.phoneNumber,
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (res) {
            console.log(res[0]);
            if (res[0] !== "") {
                createOrder(order, res[0].id);
            }
            for (let index = 0; index < myorder.length; index++) {
                const element = myorder[index];
                deleteCookie(element);
            }
            
        },
        error: function (res) {
            alert(res)
            createCustomer(customer);
        }
    });
    $('#ModalCenter').modal('hide');
    
});
function createCustomer(customer) {
    $.ajax({
        url: baseUrlApi +"/customer/create",
        type: 'POST',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(customer),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            createOrder(order, res.id);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
function createOrder(order, customerId) {
    $.ajax({
        url: baseUrlApi + "/order/create/" + customerId,
        type: 'POST',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(order),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            for (let index = 0; index < listOrderDetail.length; index++) {
                listOrderDetail[index].orderId = res.id;
                createOrderDetails(listOrderDetail[index], listOrderDetail[index].orderId, listOrderDetail[index].productId);
            }
            
        },
        error: function (ajaxContext) {
            alert(ajaxContext);
        }
    });
}
function createOrderDetails(product, orderId, productId) {
    $.ajax({
        url: baseUrlApi +"/orderdetail/create/" + orderId + "/" + productId,
        type: 'POST',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(product),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            window.location.href = "myorder.html";
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}


$('input.input-qty').each(function () {
    var $this = $(this),
        qty = $this.parent().find('.is-form'),
        min = Number($this.attr('min')),
        max = Number($this.attr('max'))
    if (min == 0) {
        var d = 0
    } else d = min
    $(qty).on('click', function () {
        if ($(this).hasClass('minus')) {
            if (d > min) d += -1
        } else if ($(this).hasClass('plus')) {
            var x = Number($this.val()) + 1
            if (x <= max) d += 1
        }
        $this.attr('value', d).val(d)
    })
});

function findProduct(array, data) {
    let start = 0;
    var product = {};
    while (start < array.length && jQuery.isEmptyObject(product) === true) {
        if (array[start].productId === parseInt(data)) {
            product = array[start];
        }
        start++;
    }
    if (jQuery.isEmptyObject(product) === false) {
        return product;
    }
    return false;
}

// sự kiện khi click cộng và trừ số lượng sản phẩm muốn mua
$('#loadProduct').on('click', '.pd', function () {
    if (this.value === "-") {
        var data = this.getAttribute("data");
        var productGot = findProduct(listOrderDetail, data);
        if (productGot.quantityOrder > 1) {
            productGot.quantityOrder--;

            var priceMines = parseInt(productGot.priceEach);
            var getOldPriceSum = parseInt($("#priceSum").attr("data"));
            var totalSum = getOldPriceSum - priceMines;

            $("#priceSum").attr("data", totalSum);
            $("#priceSum").html(formatter.format(totalSum));
            console.log(listOrderDetail);
        }

    }
    if (this.value === "+") {
        var data = this.getAttribute("data");
        var productGot = findProduct(listOrderDetail, data);
        productGot.quantityOrder++;

        var pricePlus = parseInt(productGot.priceEach);
        var getOldPriceSum = parseInt($("#priceSum").attr("data"));
        var totalSum = pricePlus + getOldPriceSum;

        $("#priceSum").attr("data", totalSum);
        $("#priceSum").html(formatter.format(totalSum));
        console.log(listOrderDetail);
    }
});